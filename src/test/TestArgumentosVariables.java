package test;

import java.util.Scanner;

public class TestArgumentosVariables {
    public static void main(String args[]){
        var consola = new Scanner(System.in);
        System.out.println("Ejemplo de argumentos variables.");
        imprimirNumeros(1,2,3,4,5,6,7,8);
    }

    private static void imprimirNumeros(int... numeros){
        System.out.println("Dentro de la función imprimir.");
        for(int i = 0; i<numeros.length; i++){
            System.out.printf("Elemento#%d: %d\n", i, numeros[i]);
        }
    }
}
